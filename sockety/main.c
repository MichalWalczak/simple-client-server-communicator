
#include<stdio.h>
#include<winsock2.h>
#include<windows.h>

#define MAX_BUFFER_LENGTH 1024
#define SERVER_PORT_NO 8074
char czy_watek_wysylania_otwarty=0;
void send_msg(SOCKET);
int main(int argc , char *argv[])
{
    WSADATA wsa;
    SOCKET s, *server_socket;
    HANDLE handle_msg_sending_thread=NULL;
    struct sockaddr_in server;
    char *message, server_reply[MAX_BUFFER_LENGTH];
    int recv_size;
    server_socket=(SOCKET *)malloc(sizeof(SOCKET)); 
    printf("\nInitialising Winsock...");
    if (WSAStartup(MAKEWORD(2,2),&wsa) != 0)
    {
        printf("Failed. Error Code : %d",WSAGetLastError());
        return 1;
    }
    printf("Initialised.");
    if((s = socket(AF_INET , SOCK_STREAM , 0 )) == INVALID_SOCKET)
    {
        printf("Could not create socket : %d" , WSAGetLastError());
    }
    printf("Socket created.\n");
    server.sin_addr.s_addr = inet_addr("192.168.0.103");
    server.sin_family = AF_INET;
    server.sin_port = htons( SERVER_PORT_NO );
 	server_socket=(SOCKET *) s;
    //Connect to remote server
    if (connect(s , (struct sockaddr *)&server , sizeof(server)) < 0)
    {
        puts("connect error");
        return 1;
    }
    puts("Connected");
     
    do//Send some data
	{
		if((handle_msg_sending_thread == NULL) && (czy_watek_wysylania_otwarty == 0))
		handle_msg_sending_thread =(HANDLE *) _beginthread( send_msg, 0, server_socket );
		  
	    if((recv_size = recv(s , server_reply , MAX_BUFFER_LENGTH , 0)) == SOCKET_ERROR)
	    {
	        puts("recv failed");
	        break;
	    }
	    //Add a NULL terminating character to make it a proper string before printing
	    server_reply[recv_size] = '\0';
	    printf("serwer - %s \n" , server_reply);
	}while(1);
    closesocket(s);
	WSACleanup();
    CloseHandle(handle_msg_sending_thread);
    return 0;
}

void send_msg(SOCKET klient)
{
	char buffer[MAX_BUFFER_LENGTH], po_literce;
	while(1)
	{
		czy_watek_wysylania_otwarty=1;
		fflush(stdin);
		fgets (buffer, MAX_BUFFER_LENGTH, stdin);
	    if ((strlen(buffer)>0) && (buffer[strlen (buffer) - 1] == '\n'))
        buffer[strlen (buffer) - 1] = '\0';
		send( klient , buffer , strlen(buffer)+1 , 0 );
		czy_watek_wysylania_otwarty=0;
	}
}
