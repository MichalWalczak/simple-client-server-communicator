#include<stdio.h>
#include<winsock2.h>
 
#define MAX_CLIENT 30
#define MAX_BUFFER_LENGTH 1024
#define SERVER_PORT_NO 8074
 
char sending_thread_open=0; 
void send_msg(SOCKET);
int main(int argc , char *argv[])
{
    WSADATA wsa;
    SOCKET master , new_socket , client_socket[MAX_CLIENT] ; 
	SOCKET s, *client_socket_ptr=NULL;
    struct sockaddr_in server, address;
    int max_clients = MAX_CLIENT , activity, addrlen, i, valread;
    int error_code;
    HANDLE handle_msg_send_thread=NULL;
    
    //Prepare the sockaddr_in structure
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = INADDR_ANY;
    server.sin_port = htons( SERVER_PORT_NO );     
    //size of our receive buffer, this is string length.
    int MAXRECV = MAX_BUFFER_LENGTH; 
    //set of socket descriptors
    fd_set readfds;     
    //1 extra for null character, string termination
    char *buffer;
    buffer =  (char*) malloc((MAXRECV + 1) * sizeof(char));   
	client_socket_ptr=(SOCKET *)malloc(sizeof(SOCKET));
    
	for(i = 0 ; i < MAX_CLIENT;i++){
        client_socket[i] = 0;
    }
  
    printf("\nInitialising Winsock...");
    if (WSAStartup(MAKEWORD(2,2),&wsa) != 0){
        printf("Failed. Error Code : %d",WSAGetLastError());
        exit(EXIT_FAILURE);
    }
    printf("Initialised.\n");     

     
    //Create a socket(adres family IPv4, TCP, nonspecyfic protocol )
    if((master = socket(AF_INET , SOCK_STREAM , 0 )) == INVALID_SOCKET){
        printf("Could not create socket : %d" , WSAGetLastError());
        exit(EXIT_FAILURE);
    }
    printf("Socket created.\n");
    //Bind
    if(bind(master ,(struct sockaddr *)&server , sizeof(server)) == SOCKET_ERROR){
        printf("Bind failed with error code : %d" , WSAGetLastError());
        exit(EXIT_FAILURE);
    }
    puts("Bind done");
 
    //Listen to incoming connections
    listen(master , 3);
     
    //Accept and incoming connection
    puts("Waiting for incoming connections...");
     
    addrlen = sizeof(struct sockaddr_in);
// petla nieskonczona **********************************************************
while(TRUE){
    //clear the socket fd set
    FD_ZERO(&readfds);

    //add master socket to fd set
    FD_SET(master, &readfds);
     
    //add child sockets to fd set
    for (  i = 0 ; i < max_clients ; i++){
        s = client_socket[i];
        if(s > 0){
            FD_SET( s , &readfds);
        }
    }
     
    //wait for an activity on any of the sockets, timeout is NULL , so wait indefinitely
    activity = select( 0 , &readfds , NULL , NULL , NULL);

    if ( activity == SOCKET_ERROR ){
        printf("select call failed with error code : %d" , WSAGetLastError());
        exit(EXIT_FAILURE);
    }
      
    //If something happened on the master socket , then its an incoming connection
    if (FD_ISSET(master , &readfds)){
        if ((new_socket = accept(master , (struct sockaddr *)&address, 
										  (int *)&addrlen))<0) 
		{
            perror("accept");
            exit(EXIT_FAILURE);
        }
      
        //inform user of socket number - used in send and receive commands
        printf("New connection , socket fd is %d , ip is : %s , port : %d \n" 
		, new_socket , inet_ntoa(address.sin_addr) , ntohs(address.sin_port));
        
        //add new socket to array of sockets
        for (i = 0; i < max_clients; i++){
            if (client_socket[i] == 0){
                client_socket[i] = new_socket;
                printf("Adding to list of sockets at index %d \n" , i);
				client_socket_ptr =(SOCKET *)new_socket;
                break;
            }
        }
    }		
	if((handle_msg_send_thread == NULL) && (sending_thread_open == 0))
	handle_msg_send_thread =(HANDLE *)_beginthread( send_msg, 0, 
													client_socket_ptr );

	//else its some IO operation on some other socket :)
    for (i = 0; i < max_clients; i++){
        s = client_socket[i];
        //if client presend in read sockets            
        if (FD_ISSET( s , &readfds)){
            //get details of the client
            getpeername(s , (struct sockaddr*)&address , (int*)&addrlen);
             //Check if it was for closing , and also read the incoming message
            //recv does not place a null terminator at the end of the string 
			//(whilst printf %s assumes there is one).
            valread = recv( s , buffer, MAXRECV, 0);
             
            if( valread == SOCKET_ERROR){
                error_code = WSAGetLastError();
                if(error_code == WSAECONNRESET){
                    //Somebody disconnected , get his details and print
                    printf("Host disconnected, ip %s , port %d \n" ,
					 inet_ntoa(address.sin_addr) , ntohs(address.sin_port));
                    //Close the socket and mark as 0 in list for reuse
                    closesocket( s );
                    client_socket[i] = 0;
                }
                else
                    printf("recv failed with error code : %d" , error_code);
			}
            if ( valread == 0){
                //Somebody disconnected , get his details and print
                printf("Host disconnected , ip %s , port %d \n" , 
				inet_ntoa(address.sin_addr) , ntohs(address.sin_port));
                //Close the socket and mark as 0 in list for reuse
                closesocket( s );
                client_socket[i] = 0;
            }else/*Echo back the message that came in*/{
                //add null character, if you want to use with printf/puts or other string handling functions
                buffer[valread] = '\0';                    
                printf("%s:%d - %s \n" , inet_ntoa(address.sin_addr) , 
									ntohs(address.sin_port), buffer);
            }
        }
    }
}
//***********************************************************************     
    closesocket(s);
    free(client_socket_ptr);
    CloseHandle(handle_msg_send_thread); 
	WSACleanup();    
	return 0;
}
void send_msg(SOCKET klient)
{
	sending_thread_open=1;
	char buffer[MAX_BUFFER_LENGTH];
	while (buffer[1]!= 'q')
	{
		sending_thread_open=1;
		fflush(stdin);
		fgets (buffer, MAX_BUFFER_LENGTH, stdin);
	    if ((strlen(buffer)>0) && (buffer[strlen (buffer) - 1] == '\n'))
        buffer[strlen (buffer) - 1] = '\0';
		
		send( klient , buffer , strlen(buffer)+1 , 0 );
	}
	sending_thread_open=0;
}

